<html>
    
    <head>

        <title>Assignment 1</title>
        <%@ page import="java.text.DateFormat" %>
        <%@ page import="java.text.SimpleDateFormat" %>
        <%@ page import="java.util.Date" %>

    </head>
    
    <body>

        <h1>
            <%
             
                Date date = new Date();  
                DateFormat dateFormat = new SimpleDateFormat("HHmmss");  
                int hour = Integer.parseInt(dateFormat.format(date));
                if(hour<120000){
                    out.print("Time for breakfast!");
                }
                else if(hour>=120000 && hour<=160000){
                    out.print("Let's have a lunch!");
                }
                else if(hour>160000){
                    out.print("Dinner time!");
                }
                             
            %>
        </h1>

    </body>

</html>